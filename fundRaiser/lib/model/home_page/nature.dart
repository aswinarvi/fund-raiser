class Nature {
  List<NatureList> trending;
  Nature({this.trending});
}

class NatureList {
  String categoryName;
  String coverImageUrl;
  String fundName;
  String daysLeft;
  int percentRaised;
  String fundDetails;
  String peopleDonated;
  NatureList(
      {this.categoryName,
      this.coverImageUrl,
      this.fundName,
      this.daysLeft,
      this.fundDetails,
      this.peopleDonated,
      this.percentRaised});
}
