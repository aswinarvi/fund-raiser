class Food {
  List<FoodList> trending;
  Food({this.trending});
}

class FoodList {
  String categoryName;
  String coverImageUrl;
  String fundName;
  String daysLeft;
  int percentRaised;
  String fundDetails;
  String peopleDonated;
  FoodList(
      {this.categoryName,
      this.coverImageUrl,
      this.fundName,
      this.daysLeft,
      this.fundDetails,
      this.peopleDonated,
      this.percentRaised});
}
