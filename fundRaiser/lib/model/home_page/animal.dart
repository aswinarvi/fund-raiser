class Animal {
  List<AnimalsList> trending;
  Animal({this.trending});
}

class AnimalsList {
  String categoryName;
  String coverImageUrl;
  String fundName;
  String daysLeft;
  int percentRaised;
  String fundDetails;
  String peopleDonated;
  AnimalsList(
      {this.categoryName,
      this.coverImageUrl,
      this.fundName,
      this.daysLeft,
      this.fundDetails,
      this.peopleDonated,
      this.percentRaised});
}
