export 'food.dart';
export 'animal.dart';
export 'nature.dart';

class Home {
  String userImageUrl;
  Home({this.userImageUrl});
}
