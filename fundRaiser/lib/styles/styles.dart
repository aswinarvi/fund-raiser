library styles;

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
part 'colors.dart';
part 'fonts.dart';
part 'radius.dart';
