part of styles;

class Radi {
  static const BorderRadiusGeometry r40Radius =
      BorderRadius.all(Radius.circular(40));
  static const BorderRadiusGeometry r20Radius =
      BorderRadius.all(Radius.circular(20));
}
