part of styles;

class AppColors {
  static final Color backgroundColor = Color(0xffd2ebfe);
  static final Color primaryComponentColor = Color(0xfffb5432);
  static final Color secondaryComponentColor = Color(0xff74a1c8);
  static final Color tertiaryComponentColor = Color(0xff000000);
}

class FontColors {
  static final Color primaryTextColor = Color(0xff000000);
  static final Color secondaryTextColor = Color(0xffffffff);
  static final Color tertiaryTextColor = Color(0xff474747);
}

class StartPageColor {
  static final Color backgroundColor = Color(0xff1a385b);
  static final Color buttonColor = Color(0xffcedce8);
}
