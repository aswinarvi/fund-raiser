part of styles;

class AppFonts {
  static TextStyle getAppFonts({
    FontWeight fontWeight,
    Color color,
    double fontsize,
  }) {
    return GoogleFonts.lato(
        textStyle: TextStyle(
      color: color,
      fontWeight: fontWeight,
      fontSize: fontsize,
    ));
  }

  static final header = getAppFonts(
      fontWeight: FontWeight.bold,
      color: FontColors.primaryTextColor,
      fontsize: 32);
  static final headingText = getAppFonts(
    fontWeight: FontWeight.bold,
    color: FontColors.primaryTextColor,
    fontsize: 20,
  );
  static final title = getAppFonts(
    fontWeight: FontWeight.normal,
    color: FontColors.secondaryTextColor,
    fontsize: 16,
  );
  static final subTitle = getAppFonts(
    fontWeight: FontWeight.normal,
    color: FontColors.primaryTextColor,
    fontsize: 14,
  );
  static final details = getAppFonts(
    fontWeight: FontWeight.normal,
    color: FontColors.tertiaryTextColor,
    fontsize: 14,
  );
}
