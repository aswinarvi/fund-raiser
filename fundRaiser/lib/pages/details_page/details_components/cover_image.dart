import 'package:flutter/material.dart';
import 'package:fundRaiser/styles/styles.dart';

class CoverImage extends StatelessWidget {
  final String coverImageUrl;
  final String categoryName;
  CoverImage({this.coverImageUrl, this.categoryName});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.5,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.474,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(35),
                  bottomRight: Radius.circular(35)),
              color: AppColors.secondaryComponentColor,
              image: DecorationImage(
                  image: NetworkImage(coverImageUrl), fit: BoxFit.cover),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 26),
            child: Align(
              alignment: Alignment.bottomRight,
              child: Container(
                child: Center(
                  child: Text(
                    categoryName,
                    style: AppFonts.title,
                  ),
                ),
                height: 40,
                width: 100,
                decoration: BoxDecoration(
                  borderRadius: Radi.r40Radius,
                  color: AppColors.primaryComponentColor,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
