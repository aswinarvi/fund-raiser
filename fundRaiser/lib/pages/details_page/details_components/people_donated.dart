import 'package:flutter/material.dart';

class PeopleDonated extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // List<String> peopleImage = [
    //   'https://images.unsplash.com/photo-1495908333425-29a1e0918c5f?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NjF8fHBsYW50c3xlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60',
    //   'https://images.unsplash.com/photo-1505209363824-91ef02e9445a?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MzF8fHBsYW50c3xlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60',
    //   'https://images.unsplash.com/photo-1446071103084-c257b5f70672?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=631&q=80',
    //   'https://images.unsplash.com/photo-1446071103084-c257b5f70672?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=631&q=80',
    //   'https://images.unsplash.com/photo-1446071103084-c257b5f70672?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=631&q=80',
    // ];
    return Container(
      // width: MediaQuery.of(context).size.width * .8,
      height: 50,
      child: Stack(
        alignment: Alignment.topLeft,
        // overflow: Overflow.visible,
        children: [
          Positioned(
            left: 0,
            child: PeopleImage(
              peopleImage:
                  'https://images.unsplash.com/photo-1495908333425-29a1e0918c5f?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NjF8fHBsYW50c3xlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60',
            ),
          ),
          Positioned(
            left: 20,
            child: PeopleImage(
              peopleImage:
                  'https://images.unsplash.com/photo-1505209363824-91ef02e9445a?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MzF8fHBsYW50c3xlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60',
            ),
          ),
          Positioned(
            left: 40,
            child: PeopleImage(
              peopleImage:
                  'https://images.unsplash.com/photo-1446071103084-c257b5f70672?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=631&q=80',
            ),
          ),
        ],
      ),
    );
  }
}

class PeopleImage extends StatelessWidget {
  final String peopleImage;
  PeopleImage({this.peopleImage});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 40,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
            image: NetworkImage(peopleImage), fit: BoxFit.cover),
        border: Border.all(color: Colors.white, width: 2),
      ),
    );
  }
}
