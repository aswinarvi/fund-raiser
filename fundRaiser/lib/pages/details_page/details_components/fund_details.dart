import 'package:flutter/material.dart';
import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';
import 'package:fundRaiser/pages/details_page/details_components/people_donated.dart';
import 'package:fundRaiser/styles/styles.dart';

class FundDetails extends StatelessWidget {
  final String fundName;
  final String daysLeft;
  final String percentRaised;
  final String fundDetails;
  final int percentraised;
  FundDetails(
      {this.fundName,
      this.daysLeft,
      this.percentRaised,
      this.fundDetails,
      this.percentraised});
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.backgroundColor,
      height: 245,
      child: Column(
        children: [
          ListTile(
            trailing: Icon(
              Icons.turned_in,
              color: AppColors.secondaryComponentColor,
              size: 25,
            ),
            title: Text(
              fundName,
              style: AppFonts.headingText,
            ),
            subtitle: Text(
              daysLeft,
              style: AppFonts.subTitle
                  .copyWith(color: FontColors.primaryTextColor),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            // child: LinearProgressIndicator(
            //   value: 80,
            //   valueColor: AlwaysStoppedAnimation<Color>(
            //     Colors.white,
            //   ),
            // ),
            child: FAProgressBar(
              size: 3,
              backgroundColor: AppColors.secondaryComponentColor,
              progressColor: AppColors.primaryComponentColor,
              maxValue: 100,
              currentValue: percentraised,
            ),
          ),
          ListTile(
            leading: Text(
              'Total raised',
              style: AppFonts.subTitle.copyWith(
                color: FontColors.primaryTextColor,
              ),
            ),
            trailing: Text(
              percentRaised,
              style: AppFonts.subTitle.copyWith(
                color: FontColors.primaryTextColor,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(17, 0, 10, 5),
            child: Text(
              fundDetails,
              style: AppFonts.details.copyWith(fontSize: 16),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 17),
            child: PeopleDonated(),
          ),
        ],
      ),
    );
  }
}
