import 'package:flutter/material.dart';
import 'package:fundRaiser/styles/styles.dart';
import './details_components/lib_details_component.dart';

class DetailsPage extends StatelessWidget {
  final String coverImageUrl;
  final String categoryName;
  final String fundName;
  final String fundDetails;
  final String daysLeft;
  final int percentRaised;
  DetailsPage(
      {this.coverImageUrl,
      this.categoryName,
      this.daysLeft,
      this.fundDetails,
      this.fundName,
      this.percentRaised});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                CoverImage(
                  coverImageUrl: coverImageUrl,
                  categoryName: categoryName,
                ),
                FundDetails(
                  fundName: fundName,
                  fundDetails: fundDetails,
                  percentRaised: '$percentRaised%',
                  daysLeft: daysLeft,
                  percentraised: percentRaised,
                ),
                Container(
                  child: Center(
                    child: Text(
                      'Contribute',
                      style: AppFonts.title,
                    ),
                  ),
                  height: MediaQuery.of(context).size.height * .075,
                  width: MediaQuery.of(context).size.width * .9,
                  decoration: BoxDecoration(
                    borderRadius: Radi.r40Radius,
                    color: AppColors.primaryComponentColor,
                  ),
                )
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * .1,
            // color: AppColors.primaryComponentColor,
            child: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.arrow_back_ios_rounded,
                  color: AppColors.primaryComponentColor,
                  size: 20,
                )),
          ),
        ],
      ),
    );
  }
}
// IconButton(
//             onPressed: () {
//               Navigator.pop(context);
//             },
//             icon: Icon(
//               Icons.arrow_back_ios_rounded,
//               color: Colors.amber,
//               size: 20,
//             )),
