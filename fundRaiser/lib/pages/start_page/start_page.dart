import 'package:flutter/material.dart';

import 'package:fundRaiser/styles/styles.dart';
import '.././start_page/start_page_components/lib_start_page_components.dart';

class StartPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: StartPageColor.backgroundColor,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Login(),
          SizedBox(
            height: 10,
          ),
          Register(),
          SizedBox(
            height: 5,
          ),
          Text(
            'forgot Password',
            style: AppFonts.details
                .copyWith(color: FontColors.secondaryTextColor.withOpacity(.3)),
          ),
          SizedBox(
            height: 20,
          )
        ],
      ),
    );
  }
}
