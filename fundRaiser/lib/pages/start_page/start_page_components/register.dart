import 'buttons.dart';
import 'package:flutter/material.dart';
import 'package:fundRaiser/styles/styles.dart';
import '../../home_page/home_page.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  bool keypressed = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => Homepage(),
            ),
          );
          setState(() {
            keypressed = !keypressed;
          });
        },
        child: Container(
          child: Stack(
            children: [
              Buttons(),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Center(
                  child: Text(
                    'Register',
                    style: AppFonts.title.copyWith(
                      color: keypressed
                          ? StartPageColor.buttonColor
                          : FontColors.primaryTextColor,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
