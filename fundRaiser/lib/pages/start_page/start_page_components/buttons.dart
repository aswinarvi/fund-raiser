import 'package:flutter/material.dart';
import 'package:fundRaiser/pages/home_page/home_page.dart';
import 'package:fundRaiser/styles/styles.dart';

class Buttons extends StatefulWidget {
  @override
  _ButtonsState createState() => _ButtonsState();
}

class _ButtonsState extends State<Buttons> {
  bool pressed = true;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: () {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => Homepage()));
          setState(() {
            pressed = !pressed;
          });
        },
        child: Container(
          height: 60,
          width: MediaQuery.of(context).size.width * .9,
          decoration: BoxDecoration(
            border: Border.all(
              color: StartPageColor.buttonColor,
              width: 2,
            ),
            borderRadius: Radi.r40Radius,
            // color: Colors.red,
            color: StartPageColor.backgroundColor,
          ),
        ),
      ),
    );
  }
}
