import 'package:flutter/material.dart';
import 'package:fundRaiser/styles/styles.dart';

class CustomAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.backgroundColor,
      height: 100,
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            height: 100,
            width: 85,
            child: Center(
              child: Icon(
                Icons.grid_view,
                color: Colors.white,
                size: 30,
              ),
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(40),
                ),
                color: AppColors.primaryComponentColor),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 20, top: 20),
            child: CircleAvatar(
              radius: 20,
              backgroundColor: AppColors.primaryComponentColor,
            ),
          ),
        ],
      ),
    );
  }
}
