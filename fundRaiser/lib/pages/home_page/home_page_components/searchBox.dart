import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fundRaiser/styles/styles.dart';

class Searchbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: Radi.r40Radius,
        border: Border.all(
          color: StartPageColor.backgroundColor,
          width: 1,
        ),
      ),
      height: 50,
      width: MediaQuery.of(context).size.width * 0.9,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            height: 50,
            width: 300,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 0, 0),
              child: TextField(
                textInputAction: TextInputAction.search,
                cursorColor: AppColors.primaryComponentColor,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Find Campaign, Charities...',
                    hintStyle: AppFonts.details.copyWith(fontSize: 13)),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Icon(
              Icons.search_rounded,
              size: 30,
              color: AppColors.tertiaryComponentColor,
            ),
          )
        ],
      ),
    );
  }
}

//
