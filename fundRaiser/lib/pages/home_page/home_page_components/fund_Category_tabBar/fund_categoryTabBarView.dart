import 'package:flutter/material.dart';
import 'package:fundRaiser/pages/home_page/home_page_components/lib_home_page_components.dart';

class CategoryTabBarView extends StatelessWidget {
  const CategoryTabBarView({@required TabController tabController})
      : _tabController = tabController;
  final TabController _tabController;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.98,
        child: TabBarView(
          children: [
            NatureTab(),
            Foodtab(),
            AnimalTab(),
          ],
          controller: _tabController,
        ),
      ),
    );
  }
}
