import 'package:flutter/material.dart';
import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';
import 'package:fundRaiser/styles/styles.dart';

class TrendingListElements extends StatelessWidget {
  final String coverImageUrl;
  final String fundName;
  final String daysLeft;
  final int percetRaised;
  TrendingListElements(
      {this.coverImageUrl, this.fundName, this.daysLeft, this.percetRaised});
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: Radi.r20Radius,
      ),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width * .6,
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Stack(
            children: [
              Container(
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: Radi.r20Radius,
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Colors.transparent,
                          Colors.black.withOpacity(.7)
                        ])),
              ),
              Container(
                height: 100,
                child: Column(
                  children: [
                    ListTile(
                      title: Text(
                        fundName,
                        style: AppFonts.title,
                      ),
                      subtitle: Text(
                        daysLeft,
                        style: AppFonts.subTitle
                            .copyWith(color: FontColors.secondaryTextColor),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      // child: LinearProgressIndicator(
                      //   value: 80,
                      //   valueColor: AlwaysStoppedAnimation<Color>(
                      //     Colors.white,
                      //   ),
                      // ),
                      child: FAProgressBar(
                        size: 3,
                        backgroundColor: AppColors.secondaryComponentColor,
                        progressColor: AppColors.primaryComponentColor,
                        maxValue: 100,
                        currentValue: percetRaised,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Total raised',
                            style: AppFonts.title.copyWith(
                              fontSize: 11,
                              color: FontColors.secondaryTextColor
                                  .withOpacity(0.5),
                            ),
                          ),
                          Text(
                            '$percetRaised%',
                            style: AppFonts.title.copyWith(
                              fontSize: 11,
                              color: FontColors.secondaryTextColor
                                  .withOpacity(0.5),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(coverImageUrl), fit: BoxFit.cover),
          borderRadius: Radi.r20Radius,
          color: AppColors.secondaryComponentColor,
        ),
      ),
    );
  }
}
