import 'package:flutter/material.dart';
import 'package:fundRaiser/model/home_page/home_page.dart';
import 'package:fundRaiser/pages/details_page/details_page.dart';
import 'package:fundRaiser/styles/styles.dart';

import '../trending_list_elements.dart';

class AnimalTab extends StatelessWidget {
  final animal = Animal(trending: [
    AnimalsList(
        coverImageUrl:
            'https://images.unsplash.com/photo-1437622368342-7a3d73a34c8f?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8YW5pbWFsfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60',
        categoryName: 'Animal',
        fundName: 'Save Plantet',
        fundDetails:
            'This will push global warming beyond the point where human and other species can cope through adaptation.',
        daysLeft: '6 days left',
        peopleDonated: '56',
        percentRaised: 80),
    AnimalsList(
        coverImageUrl:
            'https://images.unsplash.com/photo-1470093851219-69951fcbb533?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Nnx8YW5pbWFsfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60',
        categoryName: 'Animal',
        fundName: 'Save Life',
        fundDetails:
            'This will push global warming beyond the point where human and other species can cope through adaptation.',
        daysLeft: '7 days left',
        peopleDonated: '45',
        percentRaised: 80),
    AnimalsList(
        coverImageUrl:
            'https://images.unsplash.com/photo-1560343787-b90cb337028e?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTR8fGFuaW1hbHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60',
        categoryName: 'Animal',
        fundName: 'Save Earth',
        fundDetails:
            'This will push global warming beyond the point where human and other species can cope through adaptation.',
        daysLeft: '8 days left',
        peopleDonated: '26',
        percentRaised: 90)
  ]);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          ListTile(
            leading: Text(
              'Trending',
              style: AppFonts.headingText,
            ),
            trailing: Icon(Icons.more_vert_rounded),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.45,
            child: ListView.separated(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                physics: ScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DetailsPage(
                              coverImageUrl:
                                  animal.trending[index].coverImageUrl,
                              categoryName: animal.trending[index].categoryName,
                              fundDetails: animal.trending[index].fundDetails,
                              fundName: animal.trending[index].fundName,
                              daysLeft: animal.trending[index].daysLeft,
                              percentRaised:
                                  animal.trending[index].percentRaised,
                            ),
                          ),
                        );
                      },
                      child: TrendingListElements(
                        coverImageUrl: animal.trending[index].coverImageUrl,
                        fundName: animal.trending[index].fundName,
                        daysLeft: animal.trending[index].daysLeft,
                        percetRaised: animal.trending[index].percentRaised,
                      ));
                },
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    width: 10,
                  );
                },
                itemCount: animal.trending.length),
          )
        ],
      ),
    );
  }
}
