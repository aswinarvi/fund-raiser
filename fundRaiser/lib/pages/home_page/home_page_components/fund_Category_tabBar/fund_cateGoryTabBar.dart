import 'package:flutter/material.dart';
import 'package:fundRaiser/styles/styles.dart';

class CategoryTabBar extends StatelessWidget {
  const CategoryTabBar({@required TabController tabController})
      : _tabController = tabController;
  final TabController _tabController;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 5),
      child: TabBar(
        unselectedLabelColor: FontColors.primaryTextColor,
        indicator: BoxDecoration(
          borderRadius: Radi.r40Radius,
          color: AppColors.primaryComponentColor,
        ),
        tabs: <Widget>[
          Tab(
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: Radi.r40Radius,
                ),
                child: Text('Nature')),
          ),
          Tab(
            child: Text('Food'),
          ),
          Tab(
            child: Text('Animal'),
          ),
        ],
        controller: _tabController,
      ),
    );
  }
}
