import 'package:flutter/material.dart';
import 'package:fundRaiser/model/home_page/food.dart';
import 'package:fundRaiser/pages/details_page/details_page.dart';
import 'package:fundRaiser/pages/home_page/home_page_components/fund_Category_tabBar/trending_list_elements.dart';
// import 'package:fundRaiser/pages/home_page/home_page_components/fund_Category_tabBar/trending_list_elements.dart';
import 'package:fundRaiser/styles/styles.dart';

class Foodtab extends StatelessWidget {
  final food = Food(trending: [
    FoodList(
        coverImageUrl:
            'https://images.unsplash.com/photo-1505209363824-91ef02e9445a?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MzF8fHBsYW50c3xlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60',
        categoryName: 'Food',
        fundName: 'Save Plantet',
        fundDetails:
            'This will push global warming beyond the point where human and other species can cope through adaptation.',
        daysLeft: '6 days left',
        peopleDonated: '56',
        percentRaised: 80),
    FoodList(
        coverImageUrl:
            'https://images.unsplash.com/photo-1446071103084-c257b5f70672?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=931&q=80',
        categoryName: 'Food',
        fundName: 'Save Life',
        fundDetails:
            'This will push global warming beyond the point where human and other species can cope through adaptation.',
        daysLeft: '7 days left',
        peopleDonated: '45',
        percentRaised: 80),
    FoodList(
        coverImageUrl:
            'https://images.unsplash.com/photo-1495908333425-29a1e0918c5f?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NTJ8fHBsYW50c3xlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60',
        categoryName: 'Food',
        fundName: 'Save Earth',
        fundDetails:
            'This will push global warming beyond the point where human and other species can cope through adaptation.',
        daysLeft: '8 days left',
        peopleDonated: '26',
        percentRaised: 90)
  ]);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          ListTile(
            leading: Text(
              'Trending',
              style: AppFonts.headingText,
            ),
            trailing: Icon(Icons.more_vert_rounded),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.45,
            child: ListView.separated(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                physics: ScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      print('tapped');
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => DetailsPage(
                            coverImageUrl: food.trending[index].coverImageUrl,
                            categoryName: food.trending[index].categoryName,
                            fundDetails: food.trending[index].fundDetails,
                            fundName: food.trending[index].fundName,
                            daysLeft: food.trending[index].daysLeft,
                            percentRaised: food.trending[index].percentRaised,
                          ),
                        ),
                      );
                    },
                    child: TrendingListElements(
                      coverImageUrl: food.trending[index].coverImageUrl,
                      fundName: food.trending[index].fundName,
                      daysLeft: food.trending[index].daysLeft,
                      percetRaised: food.trending[index].percentRaised,
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    width: 10,
                  );
                },
                itemCount: food.trending.length),
          )
        ],
      ),
    );
  }
}
