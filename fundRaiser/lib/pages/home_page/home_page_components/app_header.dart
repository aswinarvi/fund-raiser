import 'package:flutter/material.dart';
import 'package:fundRaiser/styles/styles.dart';

class AppHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.15,
      width: MediaQuery.of(context).size.width * 0.9,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 5, left: 20, top: 10),
        child: Text(
          'Change your world with a little help!',
          style: AppFonts.header,
        ),
      ),
    );
  }
}
