import 'package:flutter/material.dart';
import 'package:fundRaiser/styles/styles.dart';
import '../home_page/home_page_components/lib_home_page_components.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  @override
  void initState() {
    _tabController = TabController(length: 3, initialIndex: 0, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      body: Column(
        children: [
          CustomAppBar(),
          AppHeader(),
          Searchbar(),
          CategoryTabBar(
            tabController: _tabController,
          ),
          Expanded(
            child: CategoryTabBarView(
              tabController: _tabController,
            ),
          )
        ],
      ),
    );
  }
}
